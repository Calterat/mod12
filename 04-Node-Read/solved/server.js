const mysql = require('mysql2');

// Create the connection to database
const connection = mysql.createConnection({
  host: 'localhost',
  port: 3306,
  // Your MySQL username
  user: 'lesson',
  // Your MySQL password
  password: 'password',
  database: 'ice_creamDB'
});

connection.connect(err => {
  if (err) throw err;
  console.log('connected as id ' + connection.threadId);
  afterConnection();
});

afterConnection = () => {
  // Write a simple query that will SELECT everything from the 'products' table
  // Log the results in the console
  //
  // YOUR CODE HERE
  //
  connection.query(`
    SELECT * FROM products
    `, (err, results, _fields) => {
      console.log(results);
    });
  connection.end();
};
