DROP DATABASE IF EXISTS books_db;
CREATE DATABASE books_db;
USE books_db;

CREATE TABLE books (
  id INTEGER PRIMARY KEY,
  authorId INTEGER,
  title VARCHAR(100)
);

CREATE TABLE authors (
  id INTEGER PRIMARY KEY,
  firstName VARCHAR(30),
  lastName VARCHAR(30)
);