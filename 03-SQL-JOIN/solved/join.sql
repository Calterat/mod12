SELECT books.id, books.title, CONCAT(authors.firstName, ' ', authors.lastName) AS Author
FROM books
INNER JOIN authors ON books.authorId=authors.id;

SELECT books.id, books.title, CONCAT(authors.firstName, ' ', authors.lastName) AS Author
FROM books
LEFT JOIN authors ON books.authorId=authors.id;

SELECT books.id, books.title, CONCAT(authors.firstName, ' ', authors.lastName) AS Author
FROM books
RIGHT JOIN authors ON books.authorId=authors.id;